/*
  Compile time fetching of an element from a list given an index
 */

#include<iostream>

template<int p, int l, int... t>
struct element_at;

template<bool, int, int, int...>
struct some_template;

template<int p, int l, int h, int... t>
struct some_template<true, p, l, h, t...>{
  const static int value = h;
};


template<int p, int l, int h, int... t>
struct some_template<false, p, l, h, t...>{
  const static int value = element_at<p, l, t...>::value;
};


template<int p, int l>
struct element_at<p, l>{
  static_assert(sizeof(p) != sizeof(l), "Not found!");
  const static int value = -99;
};

template<int p, int l, int h, int... t>
struct element_at<p, l, h, t...>{
  const static int value =  some_template<(l - (sizeof...(t) + 1)) == p, p, l, h, t...>::value;
};


int main(){
  //  std::cout << element_at<1,1,2,4,5,6,7,8,9,3>::value;
   std::cout << element_at<0,10,      4,1,0,5,6,7,8,3,9,2>::value;
   return 0;
}
