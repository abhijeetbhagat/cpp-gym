/*
  Write a unary metafunction add_const_ref<T> that returns T if it is a const reference type, and otherwise
returns T const&. Write a program to test your metafunction. Hint: you can useboost::is_same to test the
results.
 */

#include<boost/type_traits/is_reference.hpp>
#include<boost/type_traits/is_same.hpp>
#include<iostream>

template<bool, typename T> struct impl;

template<typename T>
struct add_const_ref{
  typedef typename impl<boost::is_reference<T>::value, T>::type type;
};

template<typename T>
struct impl<true, T>{
  typedef T type;
};

template<typename T>
struct impl<false, T>{
  typedef T const & type;
};


int main(){
  std::cout << boost::is_same<int const &, add_const_ref<int>::type>::value << std::endl; //prints 1
  return 0;
}
