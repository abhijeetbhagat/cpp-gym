#include<iostream>

template<typename T>
struct is_fundamental{
  static const bool value = false;
};

#define GENERATE_FUNDA(T)  template<> \
struct is_fundamental<T>{ \
  static const bool value = true; \
};

GENERATE_FUNDA(double)
GENERATE_FUNDA(char)
GENERATE_FUNDA(long)
GENERATE_FUNDA(int)
GENERATE_FUNDA(float)
GENERATE_FUNDA(bool)

int main(){
  std::cout << is_fundamental<int>::value << std::endl;
  std::cout << is_fundamental<bool>::value << std::endl;
  class P;
  std::cout << is_fundamental<P>::value << std::endl;
  return 0;
}
