#include<iostream>

template<int v>
struct int_{
  static const int value = v;
};

template<class T, class U>
struct plus{
  static const int value = T::value + U::value;
};


int main(){
  std::cout << plus<int_<5>, int_<5>>::value << std::endl;
  return 0;
}
