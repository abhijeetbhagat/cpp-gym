#include <map>
#include <iostream>
#include <mutex>
#include <thread>

template<class K, class V, class Compare = std::less<K>, 
	 class Alloc = std::allocator<std::pair<const K, V>>>
class concurrent_dict{
public:
  bool try_add(const K& _key, const V& _value){
    std::lock_guard<std::mutex> guard(_mtx);
    return _dict.insert(std::make_pair(_key, _value)).second; 
  }

  bool try_update(const K& _key, const V& _value, const V& _comp){
    std::lock_guard<std::mutex> guard(_mtx);
    auto it = _dict.find(_key);
    if(it == _dict.end()) return false;
    if(*it != _comp) return false;
    _dict[_key] = _value;
    return true;
  }
  
  size_t size() const{
    return _dict.size();
  }

private:
  V& operator[](const K& _key){
    return _dict[_key];
  }

private:
  std::map<K, V, Compare, Alloc> _dict;
  std::mutex _mtx;
};

concurrent_dict<int, std::string> gbl_dict;

void add(){
  for (int i = 0; i < 1000; i++)
  {
    gbl_dict.try_add(i, std::to_string(i));
  }
}

void update(){
}

int main(){
  std::thread t(add);
  std::thread t2(add);
  t.join();
  t2.join();

  std::cout << gbl_dict.size() << std::endl;
  return 0;
}
