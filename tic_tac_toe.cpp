#include <iostream>
#include <array>
#define RESET_CONT_AND_LOCATION cnt = 0;	\
  location = std::make_pair(-1,-1);

#define PRINT_GRID   for(int i = 0; i < 3; ++i){ \
    for(int j = 0; j < 3; ++j){ \
      std::cout << i << ' ' << j << ':' << tic_tac_toe_grid[i][j] << ", "; \
    } \
    std::cout << '\n'; \
  }

std::array<std::array<int, 3>, 3> tic_tac_toe_grid {{{0, 0, 0},
                                                     {0, 0, 0},
						     {0, 0, 0}}};

std::pair<int, int> last_mark;
std::pair<int, int> next_winning_mark;

bool check_win(const int & x, const int & y, const int & c){
  // if(y-1 >= 0){
  //   if(y+1 <= 2){
  //     if(tic_tac_toe_grid[x][y-1] == c && tic_tac_toe_grid[x][y+1] == c) return true; //vertical line
  //   }
  //   if(x-1 >= 0){
  //     if(y+1 <= 2){
  // 	if(tic_tac_toe_grid[x-1][y-1] == c && tic_tac_toe_grid[x+1][y+1] == c) return true; //backward diagonal
  //     }
  //     if(x+1 <= 2){
  // 	if(tic_tac_toe_grid[x+1][y-1] == c && tic_tac_toe_grid[x-1][y+1] == c) return true; //forward diagonal
  //     }
  //   }
  // }
  // if(x-1 >= 0){
  //   if(x+1 <= 2){
  //     if(tic_tac_toe_grid[x-1][y] == c && tic_tac_toe_grid[x+1][y] == c) return true; //horizontal line
  //   }
  // }
  // return false;
  int cnt = 0;

  if((x == 0 && y == 0) || (x == 1 && y == 1) || (x == 2 && y == 2)){
    std::cout << "check_win::back\n" ;
    for(int i =0, j = 0; i < 3; ++i, ++j){
      if(tic_tac_toe_grid[i][j] == c) 
	cnt++;
    }
    if(cnt == 3) 
      return true;
  }

  cnt = 0;

  if((x == 2 && y == 0) || (x == 0 && y == 2)){
    std::cout << "check_win::forward\n" ;
    for(int i = 2, j = 0; i >= 0; --i, ++j){
      if(tic_tac_toe_grid[i][j] == c) 
	cnt++;
    }
    if(cnt == 3)
      return true;
  }

  cnt = 0;

  for(int i = 0, j = y; i < 3; ++i){
    std::cout << "check_win::vertical\n" ;
    if(tic_tac_toe_grid[i][j] == c) 
      cnt++;
  }
  if(cnt == 3)
    return true;

  cnt = 0;
  for(int i = x, j = 0; j < 3; ++j){
    std::cout << "check_win::horizontal\n" ;
    if(tic_tac_toe_grid[i][j] == c) 
      cnt++;
  }
  if(cnt == 3)
    return true;

  return false;
}

std::pair<int, int> check_threat_and_get_thwart_location(const int & x, const int & y, const int & c){
  int cnt = 0;
  std::pair<int, int> location(-1, -1);

  if((x == 0 && y == 0) || (x == 1 && y == 1) || (x == 2 && y == 2)){
    std::cout << "check_threat_and_get_thwart_location::back\n" ;
    for(int i =0, j = 0; i < 3; ++i, ++j){
      if(tic_tac_toe_grid[i][j] == c) 
	cnt++;
      else
	location = std::make_pair(i, j);
    }
    if(cnt == 2) 
      return location;
  }

  RESET_CONT_AND_LOCATION

    if((x == 2 && y == 0) || (x == 0 && y == 2)){
      std::cout << "check_threat_and_get_thwart_location::forward\n" ;
      for(int i = 2, j = 0; i >= 0; --i, ++j){
	if(tic_tac_toe_grid[i][j] == c) 
	  cnt++;
	else
	  location = std::make_pair(i, j);
      }
      if(cnt == 2)
	return location;
    }

  RESET_CONT_AND_LOCATION

    for(int i = 0, j = y; i < 3; ++i){
      std::cout << "check_threat_and_get_thwart_location::vertical\n" ;
      if(tic_tac_toe_grid[i][j] == c) 
	cnt++;
      else
	location = std::make_pair(i, j);
    }
  if(cnt == 2)
    return location;

  RESET_CONT_AND_LOCATION

    for(int i = x, j = 0; j < 3; ++j){
      std::cout << "check_threat_and_get_thwart_location::horizontal\n" ;
      if(tic_tac_toe_grid[i][j] == c) 
	cnt++;
      else
	location = std::make_pair(i, j);
    }
  if(cnt == 2)
    return location;

  RESET_CONT_AND_LOCATION
    return location;
}

bool thwart_attack_and_check_win(const int & x, const int & y){
  tic_tac_toe_grid[x][y] = 2;
  last_mark = std::make_pair(x, y); //track the last mark
  return check_win(x, y, 2);
}

#ifdef DEBUG 
int turn = 1;
#else 
int turn = 0;
#endif

bool attack_and_check_win(const int & x, const int & y, const int & c){
  //start searching from the beginning and wherever theres a free cell, mark it
  if(turn == 0){
    turn++;
    for(int i = 0; i < 3; ++i){
      for(int j = 0; j < 3; ++j){{
	  if(tic_tac_toe_grid[i][j] == 0){
	    tic_tac_toe_grid[i][j] = 2; 
	    last_mark = std::make_pair(i, j); //track the last mark
	    return false;
	  }
	}
      }
    }
  }
  else{
    //if next_winning_mark was set then we set it and indicate victory
    if(next_winning_mark.first != -1 && next_winning_mark.second != -1){
      tic_tac_toe_grid[next_winning_mark.first][next_winning_mark.second] = 2;
      return true;
    }
    int x = last_mark.first;
    int y = last_mark.second;
    
    int cnt = 0;


    std::cout << "attack_and_check_win:back\n" ;
    //count the free cells across all lines
    int i = x, j = y;
    for(; i < 3; ++i, ++j){
      if(tic_tac_toe_grid[i][j] == 0) 
	cnt++;
    }
    if(cnt == 2) {
      next_winning_mark = std::make_pair(i-1, j-1);
      return thwart_attack_and_check_win(i-2, j-2);
    }

    cnt = 0;
    for(i = 0, j = y; i < 3; ++i){
      std::cout << "attack_and_check_win:vertical\n" ;
      if(tic_tac_toe_grid[i][j] == 0) 
	cnt++;
    }
    if(cnt == 2){
      next_winning_mark = std::make_pair(i-1, j);
      return thwart_attack_and_check_win(i-2, j);
    }
  }
  return false;
}

int main(){
  

  next_winning_mark = std::make_pair(-1, -1);


  int x, y;
  //std::cout << check_win(0, 0, 1);
  //std::cout << attack_and_check_win(0,0, 2);
  //PRINT_GRID
  // std::pair<int, int> a = check_threat_and_get_thwart_location(0, 0, 1);
  // std::cout << a.first << ' ' <<a.second;
  //--------------------------------------------------------------------  

  bool has_user_won = false;
  bool is_threat_detected = false;
  while(true){
    PRINT_GRID
    std::cout << '\n';
    std::cin >> x >> y;
    if(!tic_tac_toe_grid[x][y] == 0){
      std::cout << "That cell is already marked!"; continue;
    }
    else{
      if(next_winning_mark.first == x && next_winning_mark.second == y)
	next_winning_mark.first = next_winning_mark.second = -1;
      tic_tac_toe_grid[x][y] = 1;
    }
    has_user_won = check_win(x, y, 1);
    
    //shud check after user's turn whether we are winning
    if(next_winning_mark.first != -1 && next_winning_mark.second != -1){
      if(thwart_attack_and_check_win(next_winning_mark.first, next_winning_mark.second)){
	PRINT_GRID
	std::cout << "I win :D";
	break;
      }
    }

    if(!has_user_won){

      std::pair<int, int> location = check_threat_and_get_thwart_location(x, y, 1);
      is_threat_detected = location.first == -1 ? false : true;
      if(is_threat_detected){
	if(thwart_attack_and_check_win(location.first, location.second)){
	  PRINT_GRID
	  std::cout << "I win :D"; break;
	}
      }else{
	bool i_have_won = attack_and_check_win(x, y, 2);
	PRINT_GRID
	if(i_have_won){
	  std::cout << "I win :D"; break;
	}else{
	  continue;
	}
      }
    }else{
      std::cout << "You win :-|"; break;
    }
  }
  return 0;
}
