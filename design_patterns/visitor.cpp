#include <iostream>
#include <vector>
#include <algorithm>

//forward declarations
class Wheel;
class Body;
class Engine;
class FuelTank;
class Car;


class ICarVisitor{
public:
  virtual void Visit(Wheel*) = 0;
  virtual void Visit(Body*) = 0;
  virtual void Visit(Engine*) = 0;
  virtual void Visit(FuelTank*) = 0;
  virtual void Visit(Car*) = 0;
};

class ICarElement{
public:
  virtual void Accept(ICarVisitor*) = 0;
};

class Wheel : public ICarElement{
  std::string pos;
public:
  Wheel(std::string const & position){
    pos = position;
  }

  std::string getPosition() const{return pos;}
  
  void Accept(ICarVisitor* Visitor){
    Visitor->Visit(this);
  }
};

class Body : public ICarElement{
public:
  void Accept(ICarVisitor* Visitor){
    Visitor->Visit(this);
  }
};

class Engine : public ICarElement{
public:
  void Accept(ICarVisitor* Visitor){
    Visitor->Visit(this);
  }
};

class FuelTank : public ICarElement{
public:
  void Accept(ICarVisitor* Visitor){
    Visitor->Visit(this);
  }
};

class Car : public ICarElement{
  std::vector<ICarElement*> elements;
public:
  Car(std::vector<ICarElement*> _elements){
    elements = _elements;
  }

  void Accept(ICarVisitor* Visitor){
    std::for_each(elements.begin(), elements.end(), [=](ICarElement *element){element->Accept(Visitor);});
    Visitor->Visit(this);
  }
};


class CarPrintVisitor : public ICarVisitor{
public:
  void Visit(Wheel* element){std::cout << "Visiting " + element->getPosition() + " tyre\n"; }
  void Visit(Body* element){std::cout << "Visiting body\n";}
  void Visit(Engine* element){std::cout << "Visiting engine\n";}
  void Visit(FuelTank* element){std::cout << "Visiting FuelTank\n";}
  void Visit(Car* element){std::cout << "Visiting car\n";}
};

int main(){
  std::vector<ICarElement*> elements;
  elements.push_back(new Wheel("left front"));
  elements.push_back(new Wheel("right front"));
  elements.push_back(new Wheel("left back"));
  elements.push_back(new Wheel("right back"));
  elements.push_back(new Body());
  elements.push_back(new Engine());
  elements.push_back(new FuelTank());
  
  ICarElement *car = new Car(elements);
  ICarVisitor *visitor = new CarPrintVisitor();
  car->Accept(visitor);
  return 0;
}
