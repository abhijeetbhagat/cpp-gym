#include<iostream>

//---------------------------------------------
template<const char C>
struct get_char_as_str{
  static const std::string value;
};

template<const char C>  
const std::string get_char_as_str<C>::value = std::string("") + C;

template<int N>
struct int_to_str{
  static const std::string value;
  static const int count = 1 + int_to_str<N/10>::count;
  static constexpr const char & get(const int & i) {return value[i];}
};

template<int N>
const std::string int_to_str<N>::value = int_to_str<N/10>::value + get_char_as_str<static_cast<char>(48 + N%10)>::value;

template<>
struct int_to_str<0>{
  static const std::string value;
  static const int count = 0;
};

const std::string int_to_str<0>::value = "";
//--------------------------------------------
template<typename T>
struct get_str;

template<typename T>
struct type_descriptor{
  static const std::string value;
};

template<typename T> 
const std::string type_descriptor<T>::value = get_str<T>::value;

template<typename T>
struct type_descriptor<T*>{
  static const std::string value;
};

template<typename T>
const std::string type_descriptor<T*>::value = type_descriptor<T>::value + "*";

template<typename T>
struct type_descriptor<T&>{
  static const std::string value;
};

template<typename T>
const std::string type_descriptor<T&>::value = get_str<T>::value + "&";

template<typename T, int size>
struct type_descriptor<T[size]>{
  static const std::string value;
};

template<typename T, int size>
const std::string type_descriptor<T[size]>::value = get_str<T>::value + '[' + int_to_str<size>::value + ']';

#define GENERATE_TYPE(T) template<>\
                         struct get_str<T>{		   \
                           static const std::string value;  \
                         }; \
                         const std::string get_str<T>::value = #T;

GENERATE_TYPE(int)
GENERATE_TYPE(long)
GENERATE_TYPE(short)

template<typename T>
std::ostream & operator<<(std::ostream & o, get_str<T> t){
  return o << get_str<T>::value;
}

int main(){
  std::cout << type_descriptor<int*********>::value << std::endl;
  std::cout << type_descriptor<int[8]>::value << std::endl;
  return 0;
}
