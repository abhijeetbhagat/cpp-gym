#include<iostream>

5607055
#include<memory>

struct IScrollBar{
  virtual std::string getName() =0;
};

struct IButton{
  virtual std::string getName()=0;
};


struct IWidgetCreatorFactory{
  virtual std::shared_ptr<IScrollBar> createScrollBar() = 0;
  virtual std::shared_ptr<IButton> createButton() = 0;
};


class MotifScrollBar : public IScrollBar{
public:
  std::string getName(){return "Motif scrollbar\n";}
};

class MotifButton : public IButton{
public:
  std::string getName(){return "Motif button\n";}
};

class PlainScrollBar : public IScrollBar{
public:
  std::string getName(){return "Plain scrollbar";}
};

class PlainButton : public IButton{
public:
  std::string getName(){return "Plain button";}
};

class MotifWidgetCreatorFactory : public IWidgetCreatorFactory{
public:
  std::shared_ptr<IScrollBar> createScrollBar(){return std::shared_ptr<MotifScrollBar>(new MotifScrollBar);}
  std::shared_ptr<IButton> createButton(){return std::shared_ptr<MotifButton>(new MotifButton);}
};

class PlainWidgetCreatorFactory : IWidgetCreatorFactory{
public:
  std::shared_ptr<IScrollBar> createScrollBar(){return std::shared_ptr<PlainScrollBar>(new PlainScrollBar);}
  std::shared_ptr<IButton> createButton(){return std::shared_ptr<PlainButton>(new PlainButton);}
};



int main(){
  std::shared_ptr<IWidgetCreatorFactory> f(new MotifWidgetCreatorFactory);
  std::shared_ptr<IScrollBar> s(f->createScrollBar());
  std::shared_ptr<IButton> b(f->createButton());
  std::cout << s->getName() << '\n';
  std::cout << b->getName() << '\n';
  return 0;
}
