#include<iostream>
#include<map>
#include<cctype>
#include<chrono>

/*
  67108863
*/
int main(){
  unsigned long map{0};
  bool done = false;

  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  for(char i : "We promptly judged antique ivory buckles for the next priZe"){
    if((i > 64 && i < 91) || (i > 96 && i < 123)){
      char c = std::tolower(i);
      map = map | (1 << (c % 97)); //left shift 1 by the index position of the character in the ascii table - a : 0, b : 1, ...
      if(map == 67108863){ //all 26 bits filled
	std::cout << "panagram\n";done = true; break;
      }
    }
  }

  if(!done){
    std::cout << "not panagram\n";    
  }
  
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = start - end;
  std::cout << elapsed_seconds.count() << '\n';
  return 0;
}
