#include<iostream>
#include<boost/type_traits/is_same.hpp>

struct null{};

template<typename T, typename U>
struct type_list{
  typedef T head;
  typedef U tail;
};

template<typename T>
struct length{
  static const int size = 1 + length<typename T::tail>::size;
};

template<>
struct length<null>{
  static const int size = 0;
};

//preliminary attempt for type_at--
template<typename List, int, int, bool>
struct check;

template<typename List, int C, int N>
struct has_limit_met{
  typedef typename check<List, C, N, C==N>::type type;
};

template<typename List, int C, int N>
struct check<List, C, N, true>{
  typedef typename List::head type;
};

template<typename List, int C, int N>
struct check<List, C, N, false>{
  typedef typename has_limit_met<typename List::tail, C+1, N>::type type;
};

template<typename List, int index>
struct type_at{
  typedef typename has_limit_met<List, 0, index>::type type;
};

template<typename List>
struct type_at<List, 0>{
  typedef typename List::head type;
};
//preliminary attempt for type_at--

//second attempt
template<typename List, int index>  
struct type_at2{
  typedef typename type_at2<typename List::tail, index-1>::type type;
};

template<typename List>
struct type_at2<List, 0>{
  typedef typename List::head type;
};
//second attempt

//--preliminary attempt
template<typename List, typename T, typename U, int C>
struct is_same{
  static const int value = is_same<typename List::tail, typename List::head, U, C+1>::value;
};


template<typename List, typename T, int C>
struct is_same<List, T, T, C>{
  static const int value = C;
};

template<typename T, typename U, int C>
struct is_same<null, T, U, C>{
  static const int value = -1;
};

template<typename T, int C>
struct is_same<null, T, T, C>{
  static const int value = C;
};


template<typename List, typename T>
struct index_of{
  static const int value = is_same<typename List::tail, typename List::head, T, 0>::value;
};

//----------

template<class List, class NT>
struct append{
  typedef type_list<typename List::head, typename append<typename List::tail, NT>::new_list> new_list;
};

template<class NT>
struct append<null, NT>{
  typedef type_list<NT, null> new_list;
};

template<class T, class U>
struct append<null, type_list<T, U>>{
  typedef type_list<T, U> new_list;
};

//----------

int main(){
  typedef type_list<int, type_list<bool, type_list<short, null>>> onetype;
  typedef type_list<float, type_list<double, null>> float_type;
  static_assert(length<onetype>::size == 3, "size not 3");
  static_assert(boost::is_same<type_at2<onetype, 2>::type, short>::value, "Not same");
  static_assert(index_of<onetype, int>::value == 0, "int not at 0");
  static_assert(index_of<onetype, bool>::value == 1, "bool not at 3");
  static_assert(index_of<onetype, short>::value == 2, "short not at 2");
  static_assert(index_of<onetype, long>::value == -1, "long found");
  
  static_assert(boost::is_same<append<type_list<int, null>, short>::new_list, type_list<int, type_list<short, null>>>::value, "append1: Not same");
  static_assert(boost::is_same<append<type_list<int, type_list<short, null>>, short>::new_list, type_list<int, type_list<short, type_list<short, null>>>>::value, "append2: Not same");

  static_assert(boost::is_same<append<onetype, float_type>::new_list , type_list<int, 
		                                                                 type_list<bool, 
                                                                                          type_list<short, 
                                                                                                    type_list<float, 
                                                                                                              type_list<double,null>
                                                                                                              >
		                                                                                    >
		                                                                          >
		                                                                 >>::value, "append3: Not same");
  
  static_assert(index_of<append<onetype, float_type>::new_list, float>::value == 3, "float not found at 3");

  return 0;
}
