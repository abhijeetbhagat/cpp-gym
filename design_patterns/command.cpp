#include<iostream>
#include<vector>
#include<algorithm>

struct ICommand{
  virtual void Execute() = 0;
  virtual void UnExecute() = 0;
};

class Container{
  std::vector<std::string> container;
public:
  void Add(std::string const & str){
    container.push_back(str);
  }

  void Remove(){
    container.pop_back();
  }

  void Display(){
    std::for_each(container.begin(), container.end(), [](std::string & str){std::cout << str << '\n';});
  }
};

class EditCommand : public ICommand{
  Container* container;
  std::string str; //store the state so that when this command instance is used to call Execute(), we will have the string to be pushed
public:
  EditCommand(Container*  _container, std::string const & _str) : container(_container), str(_str){container->Add(str);}
  void Execute(){
    container->Add(str);
  }

  void UnExecute(){
    container->Remove();
  }
};

//Single instance of Receiver will be created
class Receiver{
  std::vector<ICommand*> commands;
  Container* container;
public:
    Receiver(){
      container = new Container;
    }
  void Execute(int level){
    commands[level]->Execute();
  }

  void UnExecute(int level){
    commands[level]->UnExecute();
  }

  void Write(std::string const & str){
    ICommand* c = new EditCommand(container, str);
    commands.push_back(c);
  }

  void Display(){
    container->Display();
  }
};

int main(){
  Receiver r;
  r.Write(std::string("abhi"));
  r.Write(std::string("bhagat"));
  r.Display();
  r.UnExecute(1);
  r.Display();
  std::cout << "Redoing...\n";
  r.Execute(1);
  r.Display();
  return 0;
}
