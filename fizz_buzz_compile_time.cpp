#include<iostream>

template<int N>
struct fizz_buzz{
  static const std::string value;
};

template<int N>
const std::string fizz_buzz<N>::value = (N%3 == 0 && N % 5==0 ? "fizzbuzz" : (N%3 == 0 ? "fizz" : N%5 == 0 ? "buzz" : ""));

int main(){
  std::cout << fizz_buzz<4>::value << std::endl;
  return 0;
}
