#include <iostream>
#include <array>

#define DRAW_CELL if(!_isPenUp) a[_i][_j] = 1;

std::array<std::array<int, 50>, 50> a;

enum Commands{
  PEN_UP = 1,
  PEN_DOWN,
  TURN_RIGHT,
  TURN_LEFT,
  MOVE_FORWARD,
  PRINT
};

enum Direction{
  NORTH,
  SOUTH,
  EAST,
  WEST
};

class Turtle{
public:
  Turtle(unsigned i = 0, unsigned j = 0, bool isPenUp = true) : _i{i}, _j{j}, _isPenUp{isPenUp}, _direction{EAST}{}

  void turn_left(){
    switch(_direction){
    case NORTH:
      _direction = WEST;
      break;
    case SOUTH:
      _direction = EAST;
      break;
    case EAST:
      _direction = NORTH;      
      break;
    case WEST:
      _direction = SOUTH;      
      break;
    }
  }
  
  void turn_right(){
    switch(_direction){
    case NORTH:
      _direction = EAST;
      break;
    case SOUTH:
      _direction = WEST;
      break;
    case EAST:
      _direction = SOUTH;      
      break;
    case WEST:
      _direction = NORTH;      
      break;
    }    
  }

  void pen_up(){
    _isPenUp = true;
  }

  void pen_down(){
    _isPenUp = false;
  }
  
  void move_forward(unsigned pos){
    for(unsigned i = 0; i < pos; ++i){
      switch(_direction){
      case NORTH:
	DRAW_CELL;
	_i--;
	break;
      case SOUTH:
	DRAW_CELL;
	_i++;
	break;
      case EAST:
	DRAW_CELL;
	_j++;
	break;
      case WEST:
	DRAW_CELL;
	_j--;
	break;
      }
    }
  }

  void print_grid(){
    for(int i = 0; i < 50; ++i){
      for(int j = 0; j < 50; ++j){
	std::cout << a[i][j] << ' ';
      }
      std::cout << '\n';
    }
  }
private:
  unsigned _i, _j; //col, row
  bool _isPenUp;
  Direction _direction;
};

class ICommand{
public:
  virtual void execute(Turtle & t) = 0;
  virtual ~ICommand(){}
};

class PenDownCommand : public ICommand{
public:
  void execute(Turtle & t){
    t.pen_down();
  }
};

class PenUpCommand : public ICommand{
public:
  void execute(Turtle & t){
    t.pen_up();
  }
};


class TurnLeftCommand : public ICommand{
public:
  void execute(Turtle & t){
    t.turn_left();
  }
};

class TurnRightCommand : public ICommand{
public:
  void execute(Turtle & t){
    t.turn_right();
  }
};

class MoveForwardCommand : public ICommand{
public:
  MoveForwardCommand(unsigned hops) : _hops{hops} {}

  void execute(Turtle & t){
    t.move_forward(_hops);
  }
  
private:
  unsigned _hops;
};

class PrintCommand :public ICommand{
public:
  void execute(Turtle & t){
    t.print_grid();
  }
};


class IInputHandler{
public:
  virtual ICommand* handle() = 0;
  virtual ~IInputHandler(){}
};

class ConsoleInputHandler : public IInputHandler{
public:
  ICommand* handle(){

    unsigned int c;

    std::cout << "1. Pen up\n2. Pen down\n3. Move right\n4. Move left\n5. Move forward\n6. Print\n";
    std::cin >> c;
    switch(static_cast<Commands>(c)){
    case PEN_UP:
      return new PenUpCommand();
    case PEN_DOWN:
      return new PenDownCommand();
    case TURN_RIGHT:
      return new TurnRightCommand();
    case TURN_LEFT:
      return new TurnLeftCommand();
    case MOVE_FORWARD:
      std::cout << "\nEnter the hops : ";
      unsigned int hops;
      std::cin >> hops;
      return new MoveForwardCommand(hops);
    case PRINT:
      return new PrintCommand();
    default:
      std::cout << "Invalid command. Try again.\n";
      return nullptr;
    }

  }
};


class DrawingSession{
public:
  DrawingSession(IInputHandler *handler, Turtle && t) : _handler{handler}, _t{t}{}
  
  void start(){
    while(1){
      ICommand *command = _handler->handle();
      if(command){
	command->execute(_t);
      }
      delete command;
    }
  }
  
  ~DrawingSession(){
    delete _handler;
  }

private:
  IInputHandler *_handler;
  Turtle _t;
};

int main(){
  DrawingSession(new ConsoleInputHandler, Turtle()).start();
  return 0;
}
